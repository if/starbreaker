#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

#include <irrlicht/irrlicht.h>

using namespace irr;
using namespace core;
using namespace video;
using namespace scene;


class Graphics
{
public:
        Graphics(ISceneManager *smgr, IMesh *mesh, ITexture *texture);
        ~Graphics();

        void transform(vector3df pos, quaternion orientation);
        void transformDelta(vector3df dpos, quaternion dorientation);
        void addAnimator(ISceneManager *smgr, ISceneNodeAnimator *anim);
        void setShader(s32 shader);
        
        aabbox3df getAABB() const { return this->node->getBoundingBox(); };

private:
        vector3df pos;
        quaternion orientation;
        IMeshSceneNode *node;
};


#endif
