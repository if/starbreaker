#include <stdio.h>
#include <cmath>
#include "Ship.hpp"
#include "Ammo.hpp"


Ship::Ship(ShipControl *ctrl, Body *body, Graphics *graphics)
{
        this->ctrl = ctrl;
        this->body = body;
        this->graphics = graphics;
        this->thrustStrength = 100.f; // magnitude of thrust force
        this->turnStrength = 0.5f;   // radians per frame(?) when turning
        this->fireRate = 1;
        this->fireTimer = this->fireRate;
        this->numMissiles = 50;

        if (this->graphics) {
                this->graphics->transform(this->body->getPosition(),
                                          this->body->getOrientation());
        }
}


Ship::~Ship()
{
        // hi
}

void
Ship::yaw(float k)
{
        this->body->rotate(quaternion(k * this->turnStrength, 0.0, 0.0));
}

void
Ship::pitch(float k)
{
        this->body->rotate(quaternion(0.0, k * this->turnStrength, 0.0));
}

void
Ship::roll(float k)
{
        this->body->rotate(quaternion(0.0, 0.0, k * this->turnStrength));
}



void
Ship::thrust(float k)
{
        vector3df v = this->body->getAxis();
        v = k * this->thrustStrength * v.normalize();
        this->body->applyForce(v);
}

void
Ship::brake(float k)
{
        vector3df v = this->body->getAxis();
        v = -k * this->thrustStrength * v.normalize();
        this->body->applyForce(v);
        this->body->applyForce(-k * this->body->getVelocity());
}

void
Ship::fireLasers()
{
        if (this->fireTimer > this->fireRate) {
                quaternion q = this->getOrientation();
                vector3df pos1 = vector3df(-0.5, 0, 1);
                vector3df pos2 = vector3df(+0.5, 0, 1);
                vector3df dir = this->body->getAxis().normalize();
                launchAmmo(AMMO_LASER,
                           this->getPosition() + 250 * (q * pos1),
                           quaternion(3.14/2, 0.01, 0) * q);
                launchAmmo(AMMO_LASER,
                           this->getPosition() + 250 * (q * pos2),
                           quaternion(3.14/2, -0.01, 0) * q);
                this->fireTimer = 0;
        }
}

void
Ship::launchMissile()
{
        if (this->fireTimer > this->fireRate && this->numMissiles > 0) {
                quaternion q = this->getOrientation();
                vector3df pos = vector3df(0, -1, 1);
                vector3df dir = this->body->getAxis().normalize();
                launchAmmo(AMMO_MISSILE, this->getPosition() + 500 * (q * pos), q);
                this->fireTimer = 0;
                this->numMissiles--;
        }
}


void
Ship::update(EventReceiver& e, irr::gui::ICursorControl *cursor)
{
        this->ctrl->update(*this, e, cursor);
        this->fireTimer++;

        if (this->graphics) {
                this->graphics->transform(this->getPosition(),
                                          this->getOrientation());
        }
}

vector3df
Ship::getUpVector() const
{
        return this->getOrientation() * vector3df(0.0, 1.0, 0.0);
}
