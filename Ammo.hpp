// for lasers and other ammunition (like missiles)

#ifndef __AMMO_H__
#define __AMMO_H__

#include <deque>
#include <irrlicht/irrlicht.h>
#include "Graphics.hpp"
#include "Physics.hpp"

using namespace irr::core;

enum AmmoType
{
        AMMO_LASER,
        AMMO_MISSILE,
        AMMO_NUM_TYPES,
};


struct Ammo;
typedef struct Ammo Ammo;

void initAmmoSettings(ISceneManager *smgr,
                      IMesh      *meshes[AMMO_NUM_TYPES],
                      ITexture *textures[AMMO_NUM_TYPES],
                      // note: entries with -1 signify no shader
                      s32        shaders[AMMO_NUM_TYPES]);
void launchAmmo(AmmoType type, vector3df origin, quaternion orientation);
void updateAmmo();




#endif
