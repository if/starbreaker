/* (super)class for all spaceships. */


#ifndef __SHIP_H__
#define __SHIP_H__

#include <stdio.h>
#include <irrlicht/irrlicht.h>
#include "Physics.hpp"
#include "Events.hpp"
#include "Graphics.hpp"

using namespace irr::core;


class Ship;

class ShipControl
{
public:
        ShipControl() {}
        virtual void update(Ship& ship, EventReceiver& receiver,
                            irr::gui::ICursorControl *cursor) = 0;
};


class Ship
{
public:
        // `graphics' can be NULL
        Ship(ShipControl *ctrl, Body *body, Graphics *graphics);
        ~Ship();

        void update(EventReceiver& e, irr::gui::ICursorControl *cursor);
        void roll(float k);
        void yaw(float k);
        void pitch(float k);
        
        void thrust(float k);
        void brake(float k);

        void fireLasers();
        void launchMissile();

        vector3df getPosition() const { return this->body->getPosition(); }
        vector3df     getAxis() const { return this->body->getAxis(); }
        vector3df    getEuler() const { return this->body->getEuler(); }
        vector3df getUpVector() const;
        vector3df getVelocity() const { return this->body->getVelocity(); };
        quaternion getOrientation() const { return this->body->getOrientation(); }
        float getThrustStrength() const { return this->thrustStrength; }
        float getTurnStrength() const { return this->turnStrength; }
        
private:
        Body *body;
        ShipControl *ctrl;
        Graphics *graphics;
        float thrustStrength, turnStrength;
        int fireTimer, fireRate, numMissiles;
};





class KeyboardControl : public ShipControl
{
public:
        virtual void
        update(Ship& ship, EventReceiver& e, irr::gui::ICursorControl *cursor)
                {
                        position2df c = cursor->getRelativePosition();
                        position2df d = c - position2df(0.5f, 0.5f);
                        
                        ship.yaw(d.Y);
                        ship.pitch(d.X);

                        // make cursor drift towards center of screen
                        cursor->setPosition(c + 0.1 * (position2df(0.5f, 0.5f) - c));

                        if (e.isKeyDown(irr::KEY_KEY_W)) ship.thrust(1.0);
                        if (e.isKeyDown(irr::KEY_KEY_E)) ship.thrust(10.0);
                        if (e.isKeyDown(irr::KEY_KEY_S)) ship.brake(0.5); 
                        if (e.isKeyDown(irr::KEY_KEY_A)) ship.roll(-0.5);
                        if (e.isKeyDown(irr::KEY_KEY_D)) ship.roll(0.5);
                        if (e.isKeyDown(irr::KEY_KEY_X)) ship.fireLasers();
                        // if (e.isKeyDown(irr::KEY_KEY_C)) ship.launchMissile();
                }
};


class BotControl : public ShipControl {};





#endif
