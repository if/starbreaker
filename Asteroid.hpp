#ifndef __ASTEROID_H__
#define __ASTEROID_H__

#include "Physics.hpp"
#include "Graphics.hpp"


class Asteroid
{
public:
        Asteroid(Body *body, Graphics *graphics);
        ~Asteroid();

        bool getCollisionStatus() const;
        vector3df getPosition() const { return this->body->getPosition(); };

private:
        Body *body;
        Graphics *graphics;
};





#endif
