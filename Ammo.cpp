#include <stdio.h>
#include "Ammo.hpp"

#define AMMO_LIFESPAN 100


// kind of a hack for tracking global info :/
struct AmmoSettings
{
        ISceneManager *smgr;
        IMesh      *meshes[AMMO_NUM_TYPES];
        ITexture *textures[AMMO_NUM_TYPES];
        s32        shaders[AMMO_NUM_TYPES];
};

struct AmmoSettings settings;


struct Ammo
{
        Body *body;
        Graphics *graphics;
        vector3df origin;
        bool live;
        int lifetime;
};

float payloads[AMMO_NUM_TYPES]
{
        [AMMO_LASER] = 0.00001,
        [AMMO_MISSILE] = 0.01,
};


std::deque<Ammo *> ammo;


void
initAmmoSettings(ISceneManager *smgr,
                 IMesh      *meshes[AMMO_NUM_TYPES],
                 ITexture *textures[AMMO_NUM_TYPES],
                 s32        shaders[AMMO_NUM_TYPES])
{
        settings.smgr = smgr;

        for (int i = 0; i < AMMO_NUM_TYPES; i++) {
                settings.meshes[i]   = meshes[i];
                settings.textures[i] = textures[i];
                settings.shaders[i]  = shaders[i];
        }
}

Ammo
*makeAmmo(AmmoType type, vector3df origin, quaternion orientation)
{
        Ammo *a = new Ammo();
        
        a->graphics = new Graphics(settings.smgr,
                                   settings.meshes[type],
                                   settings.textures[type]);
        if (settings.shaders[type] != -1)
                a->graphics->setShader(settings.shaders[type]);

        a->body = new Body(payloads[type], origin, a->graphics->getAABB());
        
        a->body->transform(origin, orientation);
        
        a->origin = origin;
        a->live = true;
        a->lifetime = 0;

        return a;
}


void
deleteAmmo(Ammo *a)
{
        delete a->body;
        delete a->graphics;
        delete a;        
}


void
launchAmmo(AmmoType type, vector3df origin, quaternion orientation)
{
        Ammo *a = makeAmmo(type, origin, orientation);
        ammo.push_back(a);
}

void
updateAmmo()
{
        // loop thru ammo queue, incrementing lifetimes
        for (int i = 0; i < ammo.size(); i++) {
                // if any ammo had a collision, delete it (but keep its place in queue)
                Ammo *a = ammo[i];
                
                if (!a->live) continue;

                a->graphics->transform(a->body->getPosition(),
                                       a->body->getOrientation());
                // hack: adjust direction of force
                quaternion q = quaternion(-3.14/2.0, 0.f, 0.f);
                a->body->applyForce(q * a->body->getAxis().normalize());

                // if ammo collided, destroy its body and graphics,
                // and set the live flag to false--it's basically
                // gone, but we'll delete the rest of it once its
                // lifespan is over, just to simplify things
                if (a->body->getCollisionStatus()) {
                        fprintf(stderr, "ammo collided\n");
                        a->live = false;
                        delete a->body;
                        delete a->graphics;
                        a->body = NULL;
                        a->graphics = NULL;
                }
        }

        
        // count how many objects have exceeded their lifetime; since
        // we're using a queue, the oldest objects will be at the
        // front
        int cull_count = 0;
        for (int i = 0; i < ammo.size(); i++) {
                Ammo *a = ammo[i];

                if (a->lifetime++ > AMMO_LIFESPAN) {
                        if (a->body != NULL) delete a->body;
                        if (a->graphics != NULL) delete a->graphics;
                        delete a;
                        fprintf(stderr, "culled ammo\n");
                        cull_count++;
                } else {
                        break;
                }
        }

        // remove expired ammo from the queue
        for (int i = 0; i < cull_count; i++)
                ammo.pop_front();
}
