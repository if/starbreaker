#include "Graphics.hpp"
#include "Util.hpp"



Graphics::Graphics(ISceneManager *smgr, IMesh *mesh, ITexture *texture)
{
        this->node = smgr->addMeshSceneNode(mesh);
        this->node->setMaterialTexture(0, texture);
        this->node->setMaterialFlag(EMF_LIGHTING, false);
#ifdef DEBUG
        this->node->setDebugDataVisible(true);
#endif
}


Graphics::~Graphics()
{
        this->node->remove();
};


void
Graphics::addAnimator(ISceneManager *smgr, ISceneNodeAnimator *anim)
{
        this->node->addAnimator(anim);
        anim->drop();        
}

void
Graphics::setShader(s32 shader)
{
        this->node->setMaterialType((E_MATERIAL_TYPE)shader);
}

void
Graphics::transform(vector3df pos, quaternion orientation)
{
        vector3df euler;

        this->pos = pos;
        this->orientation = orientation;
        this->orientation.toEuler(euler);
        this->node->setPosition(pos);
        this->node->setRotation(rad2deg(euler));
}

void
Graphics::transformDelta(vector3df dpos, quaternion dorientation)
{
        this->transform(this->pos + dpos, this->orientation + dorientation);
}
