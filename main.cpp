#include <stdlib.h>
#include <time.h>
#include <irrlicht/irrlicht.h>
#include <irrlicht/driverChoice.h>
#include "Ship.hpp"
#include "Events.hpp"
#include "Physics.hpp"
#include "Util.hpp"
#include "Asteroid.hpp"
#include "Ammo.hpp"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


#define NUM_SHIPS 10
#define NUM_ASTEROIDS 10

Asteroid *asteroids[NUM_ASTEROIDS];
Ship         *ships[NUM_SHIPS];
IrrlichtDevice *device;



// TODO: clean this up; currently it's copy-pasted from the shaders tutorial
class MyShaderCallBack : public video::IShaderConstantSetCallBack
{
public:

    virtual void OnSetConstants(video::IMaterialRendererServices* services,
            s32 userData)
    {
        video::IVideoDriver* driver = services->getVideoDriver();

        // set inverted world matrix
        // if we are using highlevel shaders (the user can select this when
        // starting the program), we must set the constants by name.

        core::matrix4 invWorld = driver->getTransform(video::ETS_WORLD);
        invWorld.makeInverse();

        services->setVertexShaderConstant("mInvWorld", invWorld.pointer(), 16);

        // set clip matrix

        core::matrix4 worldViewProj;
        worldViewProj = driver->getTransform(video::ETS_PROJECTION);
        worldViewProj *= driver->getTransform(video::ETS_VIEW);
        worldViewProj *= driver->getTransform(video::ETS_WORLD);

        services->setVertexShaderConstant("mWorldViewProj", worldViewProj.pointer(), 16);

        // set camera position

        core::vector3df pos = device->getSceneManager()->getActiveCamera()->getAbsolutePosition();

        services->setVertexShaderConstant("mLightPos", reinterpret_cast<f32*>(&pos), 3);

        // set light color

        video::SColorf col(0.0f,1.0f,1.0f,0.0f);

        services->setVertexShaderConstant("mLightColor",
                                          reinterpret_cast<f32*>(&col), 4);

        // set transposed world matrix

        core::matrix4 world = driver->getTransform(video::ETS_WORLD);
        world = world.getTransposed();

        services->setVertexShaderConstant("mTransWorld", world.pointer(), 16);

        // set texture, for textures you can use both an int and a float setPixelShaderConstant interfaces (You need it only for an OpenGL driver).
        s32 TextureLayerID = 0;
        services->setPixelShaderConstant("myTexture", &TextureLayerID, 1);
    }
};



void
initWorld(ISceneManager *smgr, IVideoDriver *driver)
{
        // init shaders
        if (!driver->queryFeature(video::EVDF_PIXEL_SHADER_1_1) &&
            !driver->queryFeature(video::EVDF_ARB_FRAGMENT_PROGRAM_1)) {
                fprintf(stderr, "*** Pixel shaders are disabled\n");
        }

        if (!driver->queryFeature(video::EVDF_VERTEX_SHADER_1_1) &&
            !driver->queryFeature(video::EVDF_ARB_VERTEX_PROGRAM_1)) {
                fprintf(stderr, "*** Vertex shaders are disabled\n");
        }

        video::IGPUProgrammingServices *gpu = driver->getGPUProgrammingServices();
        MyShaderCallBack *mc = new MyShaderCallBack();
        s32 laserMaterial =
                gpu->addHighLevelShaderMaterialFromFiles("shaders/opengl.vert",
                                                         "vertexMain",
                                                         video::EVST_VS_1_1,
                                                         "shaders/opengl.frag",
                                                         "pixelMain",
                                                         video::EPST_PS_1_1,
                                                         mc,
                                                         video::EMT_TRANSPARENT_ADD_COLOR);


        // init meshes
        const IGeometryCreator *geom = smgr->getGeometryCreator();

        IMesh *asteroidMeshes[20];
        for (int i = 0; i < NUM_ASTEROIDS; i++) {
                char path[64];
                snprintf(path, 64, "assets/meshes/asteroids/asteroid%d.obj", i + 1);
                asteroidMeshes[i] = smgr->getMesh(path);
        }
        
        ITexture *asteroidTextures[3] = {
                driver->getTexture("assets/textures/doshirak.jpeg"),
                driver->getTexture("assets/textures/toile-small.bmp"),
                driver->getTexture("assets/textures/syrup-large.png"),
        };
        
        for (int i = 0; i < NUM_ASTEROIDS; i++) {
                //ISceneNodeAnimator *rot = smgr->createRotationAnimator(0.1 * vector3dfrand());
                Graphics *g = new Graphics(smgr, asteroidMeshes[i], asteroidTextures[i%3]);
                Body *b = new Body(1.0, vector3df(0, 0, 0), g->getAABB());
                //g->addAnimator(smgr, rot);
                b->transform(i * 100 * vector3dfrand(), qrand() * 3.14);
                
                asteroids[i] = new Asteroid(b, g); // and it just magically shows up I guess
        }

        IMesh *ammoMeshes[AMMO_NUM_TYPES] = {
                //smgr->getMesh("assets/meshes/laser.obj"),
                geom->createCylinderMesh(3, 100, 8),
                smgr->getMesh("assets/meshes/missile.obj"),
        };
        
        ITexture *ammoTextures[AMMO_NUM_TYPES] = {
                driver->getTexture("assets/textures/red.png"),
                driver->getTexture("assets/textures/missile.png"),
        };

        s32 ammoShaders[AMMO_NUM_TYPES] = {
                laserMaterial,
                -1,
        };

        initAmmoSettings(smgr, ammoMeshes, ammoTextures, ammoShaders);
}



int
main()
{
        srand(time(NULL));
        
        // initialize physics
        initPhysics();
        
        video::E_DRIVER_TYPE driverType = video::EDT_OPENGL;

        EventReceiver receiver;
        ICursorControl *cursor;
        
        device = createDevice(video::EDT_SOFTWARE, dimension2d<u32>(1920, 1080),
                              16, true, false, false, &receiver);

        if (!device) return 1;

        // device->setResizable(true);
        
        cursor = device->getCursorControl();
        device->setWindowCaption(L"Hello world, hello Irrlicht...");
        IVideoDriver *driver = device->getVideoDriver();
        ISceneManager *smgr = device->getSceneManager();
        IGUIEnvironment *guienv = device->getGUIEnvironment();
        
        
        ISceneNode *skydome =
                smgr->addSkyDomeSceneNode(driver->getTexture("assets/textures/space2.jpg"),
                                          200, 200, 1.0, 2.0, 1000.f);
        skydome->setVisible(true);
        
        // add crosshairs in the middle of the screen
        dimension2d<u32> screenSize = driver->getScreenSize();
        printf("screen size: %d %d\n", screenSize.Width, screenSize.Height);
        IGUIImage *crosshairs =
                guienv->addImage(driver->getTexture("assets/textures/crosshairs.png"),
                                 position2d<s32>(screenSize.Width/2 - 32,
                                                 screenSize.Height/2 - 32));
        IGUIImage *cockpit =
                guienv->addImage(driver->getTexture("assets/textures/cockpit-1.png"),
                                 position2d<s32>(0, 0));

        IGUISpriteBank *sprites = guienv->addEmptySpriteBank(path("misc_sprites"));
        s32 cursorIconID = sprites->addTextureAsSprite(driver->getTexture("assets/textures/cursor.png"));
        printf("added cursor texture as sprite\n");
        SCursorSprite cursorIcon = SCursorSprite(sprites, cursorIconID);
        cursor->addIcon(cursorIcon);
        cursor->setVisible(false);

        // load an explosion animation
        u32 num_frames = 82;
        core::array<ITexture *> explosion(num_frames);
        for (u32 i = 0; i < num_frames; i++) {
                char path[64];
                snprintf(path, 64, "assets/round_vortex/frame%04d.png", i);
                explosion.push_back(driver->getTexture(path));
        }

        
        ICameraSceneNode *cam = smgr->addCameraSceneNode();
        cam->setInputReceiverEnabled(false);
        
        u32 then = device->getTimer()->getTime();
        const f32 MOVEMENT_SPEED = 5.f;
        const f32 DAMPING = 0.01;
        const f32 MAX_SPEED = 200;
        const f32 BRAKE = 0.2;
        vector3df vel;

        initWorld(smgr, driver);

        KeyboardControl playerCtrl;
        Body playerBody(1.0, vector3df(0, 600, 0),
                        aabbox3df(vector3df(0, 0, 0),
                                  vector3df(10, 10, 10)));
        Ship playerShip(&playerCtrl, &playerBody, NULL);


        while (device->run()) {
                const u32 now = device->getTimer()->getTime();
                const f32 frameDT = (f32)(now - then) / 1000.f;
                then = now;
                
                playerShip.update(receiver, cursor);
                runPhysics(frameDT);

                cam->setPosition(playerShip.getPosition());
                cam->setUpVector(playerShip.getUpVector());
                cam->setRotation(rad2deg(playerShip.getEuler()));

                // manually set target to match ship's orientation.
                vector3df pos = cam->getPosition();
                vector3df target = cam->getTarget() - cam->getAbsolutePosition();
                target.set(0, 0, max_(1.f, pos.getLength()));
                target = playerShip.getOrientation() * target;
                cam->setTarget(target + pos);

                // set camera zoom based on ship velocity, for a cool effect
                float f = playerShip.getVelocity().getLength() / 1000.f;
                f = min_(1.f, f);
                cam->setFOV(lerp(1.0, 1.5, f));

                /* ------------------------------------- */

                driver->beginScene(true, true, SColor(255, 0, 0, 0));
                smgr->drawAll(); // !
                guienv->drawAll();
                driver->endScene();
                
                // good to have an emergency off button, especially
                // when fullscreen grabs input from the window manager
                if (receiver.isKeyDown(irr::KEY_ESCAPE))
                        break;

                for (int i = 0; i < NUM_ASTEROIDS; i++) {
                        Asteroid *a = asteroids[i];
                        if (a != NULL && a->getCollisionStatus()) {
                                // TODO: factor out explosion effect
                                vector3df pos = a->getPosition();

                                ILightSceneNode *light =
                                        smgr->addLightSceneNode(0, pos, SColor(255, 0, 0, 255),
                                                                800.0);

                                IBillboardSceneNode *billboard =
                                        smgr->addBillboardSceneNode(light, dimension2df(100, 100));
                                billboard->setMaterialFlag(video::EMF_LIGHTING, false);
                                billboard->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);
                                // billboard->setMaterialTexture(0, driver->getTexture("assets/round_vortex/frame0010.png"));
                                ISceneNodeAnimator *anim =
                                        smgr->createTextureAnimator(explosion, 80, false);
                                billboard->addAnimator(anim);
                                anim->drop();

                                delete a;
                                asteroids[i] = NULL;
                        }
                }

                updateAmmo();
        }

        //endPhysics();
        device->drop();

        return 0;
}
