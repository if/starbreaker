#ifndef __EVENTS_H__
#define __EVENTS_H__

#include <irrlicht/irrlicht.h>

using namespace irr;



class EventReceiver : public IEventReceiver
{
public:
        EventReceiver() {
                        for (u32 i = 0; i < KEY_KEY_CODES_COUNT; i++)
                                KeyIsDown[i] = false;
        }


        virtual bool
        OnEvent(const SEvent& event) {
                        if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
                                KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
                        }

                        return false;
        }

        virtual bool
        isKeyDown(EKEY_CODE keyCode) const {
                        return KeyIsDown[keyCode];
        }
        
private:
        bool KeyIsDown[KEY_KEY_CODES_COUNT];
};


#endif
