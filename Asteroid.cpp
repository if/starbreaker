#include "Asteroid.hpp"

Asteroid::Asteroid(Body *body, Graphics *graphics)
{
        this->body = body;
        this->body->lock();     // make sure body is (physically) static
        this->graphics = graphics;
        this->graphics->transform(this->body->getPosition(),
                                  this->body->getOrientation());
}

Asteroid::~Asteroid()
{
        delete this->body;
        delete this->graphics;
}

bool
Asteroid::getCollisionStatus() const
{
        return this->body->getCollisionStatus();
}
