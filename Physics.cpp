#include <math.h>
#include <stdio.h>
#include "Physics.hpp"


PhysicsCommon physicsCommon;
PhysicsWorld *world;
CollisionEventListener listener;


// top-level stuff
void
initPhysics()
{
        PhysicsWorld::WorldSettings settings;
        settings.gravity = Vector3(0, 0, 0); // SPASE!
        world = physicsCommon.createPhysicsWorld(settings);
        world->setEventListener(&listener);
}

void
endPhysics()
{
        physicsCommon.destroyPhysicsWorld(world);
}

void
runPhysics(float step)
{
        world->update(step);
}


// vector and quaternion transformations (irrlicht <-> react3d)
// this names are kind of abysmal
Vector3
vdf2v(vector3df v1)
{
        Vector3 v2(v1.X, v1.Y, v1.Z);
        return v2;
}

vector3df
v2vdf(Vector3 v)
{
        return vector3df(v.x, v.y, v.z);
}

quaternion
Q2q(Quaternion q)
{
        return quaternion(q.x, q.y, q.z, q.w);
}

Quaternion
q2Q(quaternion q)
{
        return Quaternion(q.X, q.Y, q.Z, q.W);
}





Body::Body(float mass, vector3df pos, aabbox3df bbox, bool immaterial)
{
        Vector3 p = vdf2v(pos);
        Quaternion orientation = Quaternion::identity();
        Transform transform(p, orientation);
        this->body = world->createRigidBody(transform);
        this->body->setLinearDamping(0.1); // damp linear velocity
        // important to damp angular damping bc we don't want bodies
        // to spin uncontrollably when they collide with something
        // (for now)
        this->body->setAngularDamping(1.0);
        this->body->setMass(mass);
        // record a reference to self, so that we can retrace
        // collisions in the physics engine back up to our own level
        // of abstraction
        this->body->setUserData(this);
        this->body->enableGravity(false);
        this->hasCollided = false;
        
        // set up shape, including position of COM within shape
        vector3df d = bbox.MaxEdge - bbox.MinEdge;
        BoxShape *shape = physicsCommon.createBoxShape(Vector3(d.X, d.Y, d.Z));
        Collider *collider = body->addCollider(shape, Transform::identity());
        collider->setIsTrigger(immaterial);
}

Body::~Body()
{
        world->destroyRigidBody(this->body);
}


void
Body::lock()
{
        this->body->setType(BodyType::STATIC);
}

void
Body::unlock()
{
        this->body->setType(BodyType::DYNAMIC);
}


void
Body::setCollisionStatus(bool status)
{
        this->hasCollided = status;
}

void
Body::setDamping(float linear)
{
        this->body->setLinearDamping(linear);
}

void
Body::transform(vector3df pos, quaternion q)
{
        Transform t(vdf2v(pos), q2Q(q));
        this->body->setTransform(t);
}


void
Body::applyForce(vector3df _f)
{
        Vector3 f = vdf2v(_f);
        this->body->applyLocalForceAtCenterOfMass(f);
}

vector3df
Body::getPosition() const
{
        const Transform& t = this->body->getTransform();
        return v2vdf(t.getPosition());
}

vector3df
Body::getAxis() const
{
        vector3df v = this->getEuler();
        return v.rotationToDirection();
}


vector3df
Body::getEuler() const
{
        const Transform& t = this->body->getTransform();
        vector3df v;

        quaternion q = Q2q(t.getOrientation());
        q.toEuler(v);
        return v;
}

void
Body::rotate(quaternion q)
{
        Transform t = this->body->getTransform();
        Quaternion q1 = q2Q(q);
        Quaternion q2 = t.getOrientation();
        t.setOrientation(q2 * q1);
        this->body->setTransform(t);
}


quaternion
Body::getOrientation() const
{
        Transform t = this->body->getTransform();
        return Q2q(t.getOrientation());
}

vector3df
Body::getVelocity() const
{
        return v2vdf(this->body->getLinearVelocity());
}
