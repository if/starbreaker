CC=clang++
EXEC=starbreaker
CFLAGS = -std=c++11 -g -fdebug-default-version=2 -O3
LFLAGS = -L/usr/X11R6/lib -lX11 -isystem /usr/local/include/ -L/usr/local/lib -lIrrlicht -lreactphysics3d
SOURCES=*.cpp

$(EXEC): $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) $(LFLAGS) -o $@

clean:
	rm -vf *.o $(EXEC) *.core
