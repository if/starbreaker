/* this module takes care of interfacing with the "actual" physics engine. */


#ifndef __PHYSICS_H__
#define __PHYSICS_H__

#include <stdio.h>
#include <irrlicht/irrlicht.h>
#include <reactphysics3d/reactphysics3d.h>

using namespace irr::core;
using namespace reactphysics3d;



void initPhysics();
void endPhysics();
void runPhysics(float timestep);

class Body
{
public:
        Body(float mass, vector3df pos, aabbox3df bbox, bool immaterial = false);
        ~Body();

        void applyForce(vector3df f);
        void rotate(quaternion q);
        void setDamping(float linear);
        void transform(vector3df pos, quaternion q);
        void setCollisionStatus(bool status);
        void lock();
        void unlock();

        vector3df getPosition() const;
        vector3df getAxis() const;
        vector3df getEuler() const;
        vector3df getVelocity() const;
        quaternion getOrientation() const;
        bool getCollisionStatus() const { return this->hasCollided; };

private:
        RigidBody *body;
        bool hasCollided;
};


class CollisionEventListener : public EventListener
{
        virtual void onContact(const CollisionCallback::CallbackData& data) {
                for (uint i = 0; i < data.getNbContactPairs(); i++) {
                        CollisionCallback::ContactPair p = data.getContactPair(i);
                        CollisionBody *c1 = p.getBody1();
                        CollisionBody *c2 = p.getBody2();
                        Body *b1 = (Body *)c1->getUserData();
                        Body *b2 = (Body *)c2->getUserData();
                        b1->setCollisionStatus(true);
                        b2->setCollisionStatus(true);
                }
        }
};



#endif
