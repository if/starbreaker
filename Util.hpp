#ifndef __UTIL_H__
#define __UTIL_H__

#define DEBUG 1

#include <irrlicht/irrlicht.h>

using namespace irr::core;


vector3df  rad2deg(vector3df v);
vector3df  deg2rad(vector3df v);
float      frand();
vector3df  vector3dfrand();
quaternion qrand();


#endif
