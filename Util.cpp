#include <stdlib.h>
#include <math.h>
#include "Util.hpp"


vector3df
rad2deg(vector3df v)
{
        return v / (2 * PI) * 360.0f;
}

vector3df
deg2rad(vector3df v)
{
        return v / 360.0f * (2 * PI);
}

float
frand()
{
        return (float)rand()/(float)RAND_MAX;
}


vector3df
vector3dfrand()
{
        return vector3df(frand(), frand(), frand());
}

quaternion
qrand()
{
        return quaternion(frand(), frand(), frand(), frand());
}
